all: config lib 

everything: all test app test-run app app-run

.PHONY:
help:
	@echo ""
	@echo "make [all|everything|config|lib|test|app|test-run|app-run]"
	@echo ""
	@echo "    config        create ./build/ and run cmake"
	@echo "    lib           build the library"
	@echo "    test          build the unit tests"
	@echo "    test-run      run the unit tests"
	@echo "    app           build a little demo application"
	@echo "    app-run       run the demo application"
	@echo "    all           same as: config lib"
	@echo "    everything    same as all of the targets above together"
	@echo "    clean         clean lib, test and app"
	@echo ""


.PHONY:clean
clean:
	rm -f *~ src/*~
	rm -rf build/
	make -C app clean
	make -C test clean

.PHONY:config
config: # src/day_of_year.h
	mkdir build || true
	cd build && cmake -DCMAKE_BUILD_TYPE=Debug ..

#src/day_of_year.h: src/make_day_of_year_h.sh
#	./src/make_day_of_year_h.sh > src/day_of_year.h

build: config

.PHONY:lib
lib: config
	mkdir -p build
	make -C build

.PHONY:test
test: lib build
	make -C test

.PHONY:test-run
test-run: test
	make -C test run

.PHONY:app
app: lib
	make -C app

.PHONY:app-run
app-run: app
	make -C app run

