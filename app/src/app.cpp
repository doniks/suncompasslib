#include <iostream>
#include <chrono>
#include <ctime>
#include <cmath>
#include "date/date.h"
#include "suncompasslib_cpp.h"
#include "suncompasslib_internal.h"

using namespace std;
using namespace std::chrono;
using namespace date;


void usage(){
    cout << "USAGE: suncompass lat lon [time]" << endl;
}

void all(double latitude, double longitude, scl_timepoint timep, long timezone){
    cout << "lat=" << latitude << "\n"
         << "lon=" << longitude << "\n"
         << "time=" << to_YMDhms(timep) << "\n"
         << "mer=" << standard_time_meridian(longitude) << "\n"
         << "degSE=" << degrees_to_spring_equinox(timep) << "\n";
    double decl = declination(timep);
    cout << "decl=" << decl << "\n";
    cout << "eot=" << to_Hms( equation_of_time(timep) ) << "\n";
    seconds off = offset_longitude_meridian(longitude, timezone);
    cout << "offmer=" << to_Hms( off ) << "\n";
    seconds corr = time_correction(longitude, timep, timezone);
    cout << "corr=" << to_Hms( corr ) << "\n";
    seconds tod = time_of_day_s(timep);
    hours tz(timezone); //  standardTimeMeridian(longitude)); // assume tz corresponds to lon
    seconds lst = tod + tz + corr;
    cout << "lst=" << to_Hms(tod) << "+" << to_Hms(tz) << "+" << to_Hms(corr) << "=" << to_Hms(lst) << "\n";

    auto sha = lst - hours(12);
    double shadeg = fractional_hours(sha).count() * degrees_spun_per_hour;
    cout << "sha=" << to_Hms(sha) << "=" << shadeg << "\n";
    double azi = azimuth(latitude, longitude, decl, shadeg);
    cout << "azi=" << azi << " = " << azimuth(latitude, longitude, timep, timezone) << "\n";
    double ele = elevation(latitude, longitude, decl, shadeg); // timep);
    cout << "ele=" << ele << "=" << elevation(latitude, longitude, timep, timezone) << "\n";
}


int main(int argc, char** argv){
//    time_t tt = time(0);
//    printf("UTC:   %lu %s", tt, asctime(gmtime(&tt)));
//    printf("local: %lu %s", tt, asctime(localtime(&tt)));
//    // POSIX-specific
////    putenv("TZ=Asia/Singapore");
////    printf("Singapore: %s", asctime(localtime(&t)));
//    time_t ts = 0;
//    struct tm t;
//    char buf[16];
//    localtime_r(&ts, &t);
//    long offset = t.tm_gmtoff/3600;
//    strftime(buf, sizeof(buf), "%z", &t);
//    cout << "Current timezone: " << buf << std::endl;
//    cout << "Current timezone: " << offset << "\n";
//    strftime(buf, sizeof(buf), "%Z", &t);
//    cout << "Current timezone: " << buf << std::endl;
//    exit(0);
    try{
        // viewers position
        double latitude = 0;  // north-south
        double longitude = 0; // east-west
        time_t ts = 0;
        struct tm t;
        localtime_r(&ts, &t);
        long offset = t.tm_gmtoff/3600;
        cout << "System timezone: " << offset << "\n";

        // time and date
        scl_timepoint speq(sys_days(year(2020)/mar/20) + 21h + 58min);

        // now
        scl_timepoint timep = time_point_cast<seconds>(system_clock::now());

        // example
        // We’ll use as an example the
        // site of the Gateway Arch in Saint Louis, Missouri,
        // located at 38.6N, 90.2W,
        // at 7:00am on February 7, 2012
        // and find the Sun’s elevation and azimuth.
        // Central Time Zone (Jefferson City, Missouri)
        // CT = -6
        // (CDT = -5, Daylight savings)
        offset = -6;
        timep = sys_days(2012_y/2/7) + 7h -hours(offset); // local time should be 7am, so to utc -6
        latitude = 38.6;
        longitude = -90.2;
        cout << "day:" << to_Dhms( timep - sys_days(2012_y/1/1) ) << "\n";

        if ( argc >= 3 && argc <= 4 ){
//            usage();
//            exit(EXIT_SUCCESS);

            latitude = stod(argv[1]);
            longitude = stod(argv[2]);
            if ( argc >= 4 ){
                timep = from_time_t(stoi(argv[3]));
                if ( argc == 5 ){
                    offset = stoi(argv[4]);
                }
            }
        }


        all(latitude, longitude, timep, offset);

        cout << to_YMDhms(timep) << " -> " << to_YMDhms(closestSpringEquinox(timep)) << "\n";

/*
        for ( uint x = 0 ; x < 380 ; x++ )
        {
            scl_timepoint t = speq + hours(x);
            // seconds eot(equation_of_time(t));
            // cout << "eot -> " << to_YMDhms(t) << " - " << eot  << "\n";
            azimuth(20, 25, t);
            // azimuth(latitude, longitude, t);

        }
*/

    } catch(exception& ex){
        cout << "Error: " << ex.what() << endl;
    }

    exit(EXIT_SUCCESS);
}
