set -xv
SC=./build/suncompass
test -x $SC || SC=./suncompass
test -x $SC || SC=./app/build/suncompass


# Gateway Arch in Saint Louis, Missouri, located at 38.6N, 90.2W, and find the Sun’s elevation and azimuth at 7:00am on February 7, 2012.
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am January 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am February 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am March 7, 2012' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am March 22, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am April 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am May 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am June 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am July 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am August 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am September 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am October 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am November 7, 2012' )
# $SC 38.6 90.2 $( date +"%s" --date='TZ="UTC-05:00" 7:00am December 7, 2012' )
$SC 38.6 90.2 $( date +"%s" )
$SC 52.366667  4.9 # amsterdam

$SC 0 0 $( date +"%s" --date='TZ="UTC+0:00" 03:50 March 20, 2020' )
exit

march     equinox  2020 march 20 03:50
june      solstice 2020 june 20 21:44
september equinox  2020 september 22 13:31
december  solstice 2020 december 21 10:02

$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 18, 2019' )
$SC 0.00 0.00 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 18, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 19, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 20, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 21, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 23, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 24, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 25, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 26, 2019' )


$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 00:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 01:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 02:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 03:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 04:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 05:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 06:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 07:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 08:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 09:00 September 22, 2019' )
$SC 38.6 90.2 $( date +"%s" --date='TZ="UTC+0:00" 10:00 September 22, 2019' )

