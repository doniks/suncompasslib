#include <iostream>
#include <iomanip>
#include <chrono>
#include <set>
#include <ctime>
#include <cmath>
#include "date/date.h"
#include "suncompasslib_cpp.h"
#include "suncompasslib_internal.h"

using namespace std;
using namespace std::chrono;
using namespace date;

// set NAIVE to get a more naive implementation, which should be less precise, but much easier to debug
//#define NAIVE

std::set<scl_timepoint> initSpringEquinoxes(){
    std::set<scl_timepoint> se;
    /*
https://en.wikipedia.org/wiki/Equinox
event 	equinox 	solstice 	equinox 	solstice
month 	March 	June 	September 	December
year
day 	time 	day 	time 	day 	time 	day 	time
2015 	20 	22:45 	21 	16:38 	23 	08:21 	22 	04:48
2016 	20 	04:30 	20 	22:34 	22 	14:21 	21 	10:44
2017 	20 	10:28 	21 	04:24 	22 	20:02 	21 	16:28
2018 	20 	16:15 	21 	10:07 	23 	01:54 	21 	22:23
2019 	20 	21:58 	21 	15:54 	23 	07:50 	22 	04:19
2020 	20 	03:50 	20 	21:44 	22 	13:31 	21 	10:02
2021 	20 	09:37 	21 	03:32 	22 	19:21 	21 	15:59
2022 	20 	15:33 	21 	09:14 	23 	01:04 	21 	21:48
2023 	20 	21:24 	21 	14:58 	23 	06:50 	22 	03:27
2024 	20 	03:07 	20 	20:51 	22 	12:44 	21 	09:20
2025 	20 	09:02 	21 	02:42 	22 	18:20 	21 	15:03
*/

    se.insert( sys_days(2015_y/3/20) + 22h + 45min);
    se.insert( sys_days(2016_y/3/20) +  4h + 30min);
    se.insert( sys_days(2017_y/3/20) + 10h + 28min);
    se.insert( sys_days(2018_y/3/20) + 16h + 15min);
    se.insert( sys_days(2019_y/3/20) + 21h + 58min);
    se.insert( sys_days(2020_y/3/20) +  3h + 50min);
    se.insert( sys_days(2021_y/3/20) +  9h + 37min);
    se.insert( sys_days(2022_y/3/20) + 15h + 33min);
    se.insert( sys_days(2023_y/3/20) + 21h + 24min);
    se.insert( sys_days(2024_y/3/20) +  3h +  7min);
    se.insert( sys_days(2025_y/3/20) +  9h +  2min);

    return se;
}


scl_timepoint closestSpringEquinox(scl_timepoint timep){
    static std::set<scl_timepoint> SpringEquinoxes = initSpringEquinoxes();
    // find the first element in the container which goes after timep,
    // or set::end if no element goes after val.
    auto it = SpringEquinoxes.upper_bound(timep);
    if ( it == SpringEquinoxes.end() ) {
        // return the last element since that is closest
        return *SpringEquinoxes.rbegin();
    } else if ( it == SpringEquinoxes.begin() ) {
        // timep < it and it is the first, so return it
        return *it;
    } else {
        // check whether timep is closer to it or to its predecessor
        auto prev = it;
        prev--;
        auto delta_it = *it - timep;
        auto delta_prev = timep - *prev;
        if (delta_it < delta_prev)
            return *it;
        else
            return *prev;
    }
}


double degrees_to_spring_equinox(scl_timepoint timep){
    seconds d = timep - closestSpringEquinox(timep);
    fractional_days delta_days = d;
    double delta_degs = degrees_per_day * delta_days.count();
    delta_degs = fmod(delta_degs, 360.0); // 360 degrees

    return delta_degs;
}



/**
 * The Sun’s declination varies through the year as the sun’s daily high point
 * moves north and south of the equator due to the tilt of the Earth’s axis.
 * The declination is always between 23.45° north and 23.45° south.  It is the
 * same at all points on the globe.
 *
 * It is
 *   0°    at the spring equinox
 *  23.45° north of the equator (the Tropic of Cancer) at the summer solstice, and
 *   0     at the autumn equinox
 * -23.45° south of the equator (the Tropic of Capricorn) at the winter solstice.
 *
 * The formula is 23.45 sin(B), where
 * B = 360/365 (d − 81);
 * in this calculation,
 * 360 is the number of degrees in a circle,
 * 365 is the number of days in the year (use 366 in leap years),
 * d is the day number with January 1st = 1, January 2nd = 2, and so on, and
 * 81 is the day number of March 22nd, which is the spring equinox.
 */
extern double declination(time_t seconds_since_epoc){
    auto t = from_time_t(seconds_since_epoc);
    return declination(t);
}
extern double declination(scl_timepoint tp){
    double deg = degrees_to_spring_equinox(tp);
    return declination_solstice_degrees * sin( deg2rad(deg) );
}


/*
The equation of time corrects for (“equates”) the eccentricity of the Earth’s orbit around the Sun and the tilt of the Earth on its axis.

The equation of time repeats each year, and is based on the number of days since the beginning of the year.

The formula is 9.87 sin(2B) − 7.53 cos(B) − 1.5 sin(B),
where B is the same calculation we made to compute the declination.

The equation of time is given in minutes.

Given the equation of time, we can calculate the time correction factor that adjusts between local clock time and local sun time, which adjusts for variation within a time zone due to longitude and also incorporates the equation of time. The formula is

4(long − 15t) + eot,

where 4 = 24 × 60 ÷ 360 is the number of minutes it takes the Earth to spin 1 degree,
15 = 360 ÷ 24 is the number of degrees the Earth spins in one hour,
long is the longitude of the current location,
t is the number of hours east (positive) or west (negative) the local time zone is from the Greenwich meridian
(remember to add 1 hour if daylight saving time is in effect), and
eot is the equation of time.

The time correction is given in minutes.

https://programmingpraxis.com/2012/02/07/solar-compass/
https://pvcdrom.pveducation.org/SUNLIGHT/SOLART.HTM
 */
seconds equation_of_time(scl_timepoint timep){
    double rad = deg2rad( degrees_to_spring_equinox(timep) );
    double eot_min = 9.87 * sin( 2 * rad )
            - 7.53 * cos( rad )
            - 1.5 * sin( rad );
    seconds eot( (int) (eot_min * 60.0) );
    //cout << "eot: " << eot_min << " " << eot << "\n";
    return eot;
}

/**
 * longitude (London: (lat,lon)=(51.50853, -0.12574))
 *
 * returns the minutes to correct from the local time zone to the local sun time
 */
seconds offset_longitude_meridian(double longitude, double timezone){
    // int timezone = standard_time_meridian(longitude);
    double lon_meridian = timezone * degrees_spun_per_hour;
    double offset_lon = longitude - lon_meridian;
    assert( seconds_to_spin_one_degree == minutes{4} );
    // FIXME: how to do this in proper chrono style without .count()?
    double x = (double)seconds_to_spin_one_degree.count();
    double secs = x * offset_lon;
    seconds correction( (int) secs );
    //cout << "lstc:" << longitude << " " << timezone << " " << offset_lon << " " << x << " " << secs << " " << correction << "\n";
    return correction;
}


/**
 * longitude in decimals
 *
 * returns the LSTM Local Standard Time Meridian, ie the standard time offset at excatly the meridian in the middle of the time zone
 * returns as hours offset off GMT (e.g., GMT=0, CET=+1, EST=-5, etc)
 */
extern int standard_time_meridian(double longitude){
    double hours = longitude / degrees_spun_per_hour;
    return (int)round(hours);
}




/*
the local solar time is

the local time
plus the time correction factor divided by 60;

the local solar time is measured in hours, with a fractional part indicating the minutes after the hour.
*/
double local_solar_time_hours(double longitude, time_t seconds_since_epoc, double timezone){
    seconds lst = local_solar_time(longitude, from_time_t(seconds_since_epoc), timezone );
    return fractional_hours(lst).count();
}

seconds local_solar_time(double longitude, scl_timepoint timep, double timezone){
    auto tod = time_of_day_s(timep);
    hours tz((int)timezone);
    auto corr = time_correction(longitude, timep, timezone);
    seconds lst = tod + tz + corr;
    return lst;
}



seconds time_correction(double longitude, scl_timepoint timep, double timezone){
    seconds longitude_correction = offset_longitude_meridian(longitude, timezone);
#ifndef NAIVE
    return longitude_correction + equation_of_time(timep);
#else
    return longitude_correction;
#endif
}


/**
 * The solar hour angle is
 * 0° at local solar noon,
 * negative in the solar morning and
 * positive in the solar afternoon.
 *
 * The solar hour angle changes by 15° every hour as the Earth spins
 */

double solar_hour_angle(double longitude, scl_timepoint timep, double timezone){
    auto lst = local_solar_time(longitude,timep,timezone);
    auto sha = lst - hours(12);
    double shadeg = fractional_hours(sha).count() * degrees_spun_per_hour;
    return shadeg;
}
double solar_hour_angle(double longitude, time_t seconds, double timezone){
    return solar_hour_angle(longitude, from_time_t(seconds), timezone);
}


/*
 *
 *  Elevation (or altitude) is the angle above the horizon.
 *
 * elevation is calculated by the formula
sin-1(sin(dec) sin(lat) + cos(dec) cos(lat) cos(hra))
where
dec is the declination,
lat is the latitude of the current location, and
hra is the solar hour angle.
*/
double elevation(double latitude, double longitude, double declination, double solar_hour_angle){
    double lat = deg2rad(latitude);
    double lon = deg2rad(longitude);
    double dec = deg2rad(declination);
    double sha = deg2rad(solar_hour_angle);

    double sdec = sin(dec);
    double cdec = cos(dec);
    double slat = sin(lat);
    double clat = cos(lat);

    double csha = cos(sha);

    double x = sdec * slat + cdec * clat * csha;
    double ele = asin(x);
    return rad2deg(ele);
}

double elevation(double latitude, double longitude, scl_timepoint timep, double timezone){
    return elevation(latitude, longitude, declination(timep), solar_hour_angle(longitude, timep, timezone));
}

double elevation(double latitude, double longitude, time_t seconds, double timzeone){
    return elevation(latitude, longitude, from_time_t(seconds), timezone);
}

/*
 *
 * Azimuth is measured eastward from the north point (sometimes from the
 * south point) of the horizon
 *
 * The azimuth is calculated by the formula
cos−1( (sin(dec) cos(lat) − cos(dec) sin(lat) cos(hra) ) / cos(α))
where α, which is the elevation angle at solar noon, is
90 − lat + dec in the northern hemisphere and
90 + lat − dec in the southern hemisphere.
*/

double azimuth(double latitude, double longitude, double declination, double solar_hour_angle){

    double lat = deg2rad(latitude);
    double lon = deg2rad(longitude);
    double dec = deg2rad(declination);
    double sha = deg2rad(solar_hour_angle);

    double sdec = sin(dec);
    double cdec = cos(dec);
    double slat = sin(lat);
    double clat = cos(lat);

    double csha = cos(sha);

    double alpha = 0;
    // FIXME: detect northern / southern hemisphere
    // lat > 0 ?
    if (true){
        // North
        alpha = deg2rad( 90 - latitude + declination );
    } else {
        // South
        alpha = deg2rad( 90 + latitude - declination );
    }
    double calp = cos(alpha);

    double nom = sdec * clat - cdec * slat * csha;
    double div = nom/calp;
    double x = min(max(div, -1.0), 1.0);
    double rad = acos(x);
    double deg = rad2deg(rad);
//    cout << "dec=" << dec << " "
//         << "lat=" << lat << " "
//         << "sdec=" << sdec << " "
//         << "cdec=" << cdec << " "
//         << slat << " "
//         << clat << " "
//         << "sha=" << sha << " "
//         << "csha=" << csha << " "
//         << "alp=" << alpha << "=(" << rad2deg(alpha) << ") "
//         << "calp=" << calp << " "
//         << "x=" << x << " "
//         << div << " "
//         << rad << " "
//         << deg << "\n";
    return deg;
}

double azimuth(double latitude, double longitude, scl_timepoint timep, double timezone){
    return azimuth(latitude, longitude, declination(timep), solar_hour_angle(longitude, timep, timezone));
}
double azimuth(double latitude, double longitude, time_t seconds, double timezone){
    return azimuth(latitude, longitude, from_time_t(seconds), timezone);
}



scl_timepoint from_time_t(time_t t)
{
    return time_point_cast<scl_timepoint::duration>(system_clock::from_time_t(t));
}
time_t to_time_t(scl_timepoint tp){
    return system_clock::to_time_t(tp);
}



void to_Hms(ostream& out, seconds s){
    int64_t c = s.count();
    bool neg = (c < 0);
    if (neg)
        c = -c;

    int16_t secs = c % 60;
    c -= secs;
    c = c / 60;

    int16_t mins = c % 60;
    c -= mins;
    c = c / 60;

    if (neg)
        out << "-";
    out << setfill('0') << setw(2) << c << ":"
       << setfill('0') << setw(2) << mins << ":"
       << setfill('0') << setw(2) << secs;
}

string to_Hms(seconds s){
    ostringstream ss;
    to_Hms(ss, s);
    return ss.str();
}

string to_Dhms(seconds s){
    ostringstream ss;
    int64_t c = s.count();

    int16_t secs = c % 60;
    c -= secs;
    c = c / 60;

    int16_t mins = c % 60;
    c -= mins;
    c = c / 60;

    int16_t hrs = c % 24;
    c -= hrs;
    c = c / 24;

    ss << c << "d+"
       << setfill('0') << setw(2) << hrs << ":"
       << setfill('0') << setw(2) << mins << ":"
       << setfill('0') << setw(2) << secs;
    return ss.str();
}

string to_YMDhms(scl_timepoint tp){
    sys_days d = floor<days>(tp);
    ostringstream ss;
    ss << d << " ";
    seconds sec = tp - d;
    to_Hms(ss, sec);
    return ss.str();
}

// FIXME there is a date::time_of_day
seconds time_of_day_s(scl_timepoint tp){
    sys_days d = floor<days>(tp);
    seconds s = tp - d;
    return s;
}
double time_of_day_h(scl_timepoint tp){
    return (double) time_of_day_s(tp).count() / 60.0 / 60.0;
}

double duration_h(seconds s){
    duration<double, std::ratio< 3600> > fractional_hours = s;
    return fractional_hours.count();
}
