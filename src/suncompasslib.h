#ifndef SUNCOMPASSLIB_H
#define SUNCOMPASSLIB_H

#ifdef __cplusplus
#include <ctime>
extern "C" {
#endif


extern double declination(time_t seconds);

extern int standard_time_meridian(double longitude);

/* the local solar time, a time of day "tod" in seconds */
extern double local_solar_time_hours(double longitude, time_t seconds, double timezone);

extern double solar_hour_angle(double longitude, time_t seconds, double timezone);

extern double elevation(double latitude, double longitude, time_t seconds, double timezone);

extern double azimuth(double latitude, double longitude, time_t seconds, double timezone);

#ifdef __cplusplus
}
#endif

#endif
