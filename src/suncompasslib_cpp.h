#ifndef SUNCOMPASSLIB_CPP_H
#define SUNCOMPASSLIB_CPP_H

#include <chrono>
#include <string>
#include "suncompasslib.h"

// normally suncompasslib works with seconds, ie std::chrono::seconds and the following timepoint
typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds> scl_timepoint;

// duration as a fraction of days
typedef std::chrono::duration<double, std::ratio_multiply<std::ratio<24>, std::chrono::hours::period> > fractional_days;

typedef std::chrono::duration<double, std::ratio_multiply<std::ratio<1>, std::chrono::hours::period> > fractional_hours;

void to_Hms(std::ostream& out, std::chrono::seconds s);
std::string to_Hms(std::chrono::seconds s);
std::string to_Dhms(std::chrono::seconds s);
std::string to_YMDhms(scl_timepoint tp);

std::chrono::seconds time_of_day_s(scl_timepoint tp);
double time_of_day_h(scl_timepoint tp);
double duration_h(std::chrono::seconds s);

scl_timepoint from_time_t(time_t t);
time_t to_time_t(scl_timepoint tp);

scl_timepoint closestSpringEquinox(scl_timepoint timep);

extern double declination(scl_timepoint timep);

extern double degrees_to_spring_equinox(scl_timepoint timep);

double solar_hour_angle(double longitude, scl_timepoint timep, double timezone);

std::chrono::seconds offset_longitude_meridian(double longitude, double timezone);

std::chrono::seconds time_correction(double longitude, scl_timepoint timep, double timezone);

std::chrono::seconds local_solar_time(double longitude, scl_timepoint timep, double timezone);

extern double elevation(double latitude, double longitude, scl_timepoint timep, double timezone);

extern double azimuth(double latitude, double longitude, scl_timepoint timep, double timezone);

#endif
