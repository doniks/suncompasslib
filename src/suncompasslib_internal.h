#ifndef SUNCOMPASSLIB_INTERNAL_H
#define SUNCOMPASSLIB_INTERNAL_H

#include <chrono>
#include <cmath>
#include "date/date.h"
#include "suncompasslib.h"
#include "suncompasslib_cpp.h"

/** round with precision */
double roundp(double x, uint p){
    double scale = 10.0;
    scale = pow(scale, p);
    return round( x * scale ) / scale;
}

double deg2rad(double deg){
    return deg / 180.0 * M_PI;
}

double rad2deg(double rad){
    return rad / M_PI * 180.0;
}


/** convert an angle from (degrees, minutes) to decimal degrees */
double deg_min2deg_dec(double degrees, double minutes) {
    return degrees + minutes / 60.0;
}


/** how fast does the earth rotate around the sun */
static const double degrees_per_day = 360.0 / 365.0; // TODO: do we need to be smart in leap years?

/** maximum declination of the sun at the time of solstice (quarter of a year after equinox) */
static const double declination_solstice_degrees = deg_min2deg_dec(23, 44);

/** how fast does the earth spin around it's axis */
static constexpr double degrees_spun_per_hour = 360.0 / 24.0; // 15.0
static constexpr double minutes_to_spin_one_degree = 1.0 / degrees_spun_per_hour * 60.0; // 4.0

constexpr std::chrono::seconds seconds_to_spin_one_degree{ (int) (1.0 / degrees_spun_per_hour * 60.0 * 60.0) }; // 4 min = 240s

std::chrono::seconds equation_of_time(scl_timepoint timep);

std::ostream& operator<<(std::ostream& out, std::chrono::seconds s){
    out << s.count() << "s";
    return out;
}

double azimuth(double latitude, double longitude, double decdeg, double shadeg);

double elevation(double latitude, double longitude, double declination, double solar_hour_angle);


#endif
