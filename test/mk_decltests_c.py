#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# https://moontracks.com/daily-declinations.html


import numpy as np
import sys
import calendar

line=input()
#print(line)

for line in sys.stdin:
    items=line.split()
    sign = 1
    if items[2][0] == "S":
        sign = -1
    [deg, min] = items[2].replace('N', '').replace('S', '').replace("'","").split("°")
    degdec = sign * ( int(deg) + int(min) / 60 )
    #print(items[0], items[1], degdec)
    print("TESTDOUBLE(declination( sys_days(2020_y/",
        list(calendar.month_abbr).index(items[0]),
        "/",
        int(items[1]),
        ")),",
        degdec,
        ");")


#month=input()
#day=input()
#sun=input()
#mercury=input()
#venus=input()
#mars=input()
#jupiter=input()
# print(month, day, sun)
