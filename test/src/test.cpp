#include <iostream>
#include <cassert>
#include <cmath>
#include "date/date.h"
#include "suncompasslib.h"
#include "suncompasslib_cpp.h"
#include "suncompasslib_internal.h"

//#include "munit/munit.h"

using namespace std;
using namespace std::chrono;
using namespace date;

constexpr double math_epsilon = 0.00000001;
double test_epsilon = math_epsilon;

bool epsequal(double x, double y, double epsilon = test_epsilon){
    return( x <= y + epsilon ) && (x >= y - epsilon );
}

#define STR(str) #str
#define TESTINT(func, y) do { long long x = func ; if (x == y){ printf("PASS(%s == %s)\n", STR(func), STR(y)); }else{printf("FAIL(%s == %lld != %s)\n", STR(func), x, STR(y));}  } while (0)
#define TESTDOUBLE(func, y) do { double x = func ; if (epsequal(x,y)){ printf("PASS(%s == %s)\n", STR(func), STR(y)); }else{printf("FAIL(%s == %lf != %s)\n", STR(func), x, STR(y));}  } while (0)




int main(){ // int argc, char** argv){

    // the external interface works with time_t values
    // make sure there is a common understanding, ie epoch:
    TESTINT(scl_timepoint(sys_days(year(1970)/1/1)).time_since_epoch().count(),0);


    //time_t equinox2019spring = make_time_t("2019-03-20T 20:58");
//    scl_timepoint equinox2019spring = sys_days(2019_y/3/20) + 20h + 58min;


    printf("TEST declination\n");
//    double max_decl = 23.44;
//    max_decl = 23.4;
//    cout << "max_decl=" << max_decl << endl;
//    TESTDOUBLE(roundp(declination(get_time_t(2016,  6, 20)),1),  max_decl);
//    TESTDOUBLE(roundp(declination(get_time_t(2018, 12, 22)),1), -max_decl);
//    TESTDOUBLE(roundp(declination(get_time_t(2019,  6, 21)),1),  max_decl);
//    TESTDOUBLE(roundp(declination(get_time_t(2019, 12, 22)),1), -max_decl);

    test_epsilon = 0.3;
#include "decltests.c.in"
    test_epsilon = math_epsilon;


//    TESTDOUBLE( roundp(declination(make_time_t("1900-01-01T 07:03")),3), roundp(0, 3) );

//    TESTDOUBLE( roundp(declination(make_time_t("2019-08-31T 00:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-08-31T 06:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-08-31T 12:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-08-31T 18:00")),3), roundp(0, 3) );

//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 00:00")),3), roundp(8.46, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 01:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 02:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 02:01")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 02:02")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 06:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 12:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 18:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-01T 23:59")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-02T 00:00")),3), roundp(0, 3) );

//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-26T 00:00")),3), roundp(0, 3) );
//    TESTDOUBLE( roundp(declination(make_time_t("2019-09-27T 12:00")),3), roundp(-1.75, 3) );

    // TODO
    // I have a hunch that the sign of the declination is wrong ... and probably the qml app now has a bug on top of that flipping the sign again :-P
    //time_t _time_t = 
    //assert(declination());
    


    printf("TEST standardTimeMeridian\n");
    TESTINT(standard+time_meridian(-181.0), -12); // not actually a legal lon, but lets make sure the conversion doesn't freak out immediately'
    TESTINT(standard+time_meridian(-180.0  ), -12);
    TESTINT(standard+time_meridian(-179.0  ), -12);
    TESTINT(standard+time_meridian(-174.0  ), -12);
    TESTINT(standard+time_meridian(-173.0  ), -12);
    TESTINT(standard+time_meridian(-172.5  ), -12); // exactly on the time zone border
    TESTINT(standard+time_meridian(-172.0  ), -11);
    TESTINT(standard+time_meridian(-170.0  ), -11);
    TESTINT(standard+time_meridian(-167.0  ), -11);
    TESTINT(standard+time_meridian(-166.0  ), -11);
    TESTINT(standard+time_meridian(-165.0  ), -11);
    TESTINT(standard+time_meridian(-160.0  ), -11);
    TESTINT(standard+time_meridian(-150.0  ), -10);
    TESTINT(standard+time_meridian( -70.0  ),  -5);
    TESTINT(standard+time_meridian( -15.0  ),  -1);
    TESTINT(standard+time_meridian( -10.0  ),  -1);
    TESTINT(standard+time_meridian(  -7.501 ), -1);
    TESTINT(standard+time_meridian(  -7.5   ), -1); // zone border
    TESTINT(standard+time_meridian(  -7.499 ),  0);
    TESTINT(standard+time_meridian(  -1     ),  0);
    TESTINT(standard+time_meridian(   0     ),  0);
    TESTINT(standard+time_meridian(   1     ),  0);
    TESTINT(standard+time_meridian(   7.499 ),  0);
    TESTINT(standard+time_meridian(   7.5   ),  1); // zone border
    TESTINT(standard+time_meridian(   7.501 ),  1);
    TESTINT(standard+time_meridian(  15     ),  1);
    TESTINT(standard+time_meridian(  22.5   ),  2); // zone border


    printf("TEST sun_minutes\n");
    test_epsilon = 1;
    TESTINT(offset_longitude_meridian(  0.0 ).count(),seconds(0).count());
    TESTINT(offset_longitude_meridian(  7.5 ).count(),seconds(minutes(-30)).count());
    TESTINT(offset_longitude_meridian( 15.0 ).count(),seconds(0).count());
    TESTINT(offset_longitude_meridian( 30.0 ).count(),seconds(0).count());
    TESTINT(offset_longitude_meridian(165.0 ).count(),seconds(0).count());
    TESTDOUBLE(offset_longitude_meridian(172.49999).count(),1800); // seconds(minutes(30)).count());
    TESTINT(offset_longitude_meridian(172.5 ).count(),seconds(minutes(-30)).count());
    TESTDOUBLE(offset_longitude_meridian(172.500001).count(),-1800); // seconds(minutes(-30)).count());
    TESTINT(offset_longitude_meridian(180.0 ).count(),seconds(0).count());

    TESTINT(offset_longitude_meridian( -7.5 ).count(),seconds(minutes(30)).count());
    TESTINT(offset_longitude_meridian(-15.0 ).count(),seconds(0).count());
    test_epsilon = math_epsilon;


//    printf("TEST solar hour angle\n");
//    TESTDOUBLE(hours_in_day(make_time_t("2019-03-20T 00:00")),  0);
//    TESTDOUBLE(hours_in_day(make_time_t("2019-03-20T 06:00")),  6);
//    TESTDOUBLE(hours_in_day(make_time_t("2019-03-20T 12:00")), 12);
//    TESTDOUBLE(hours_in_day(make_time_t("2019-03-20T 18:00")), 18);
//    TESTDOUBLE(hours_in_day(make_time_t("2019-03-21T 00:00")),  0);


    printf("TEST solar hour angle\n");
    test_epsilon = 0.0001;
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/20)) +  0h), -180.0);
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/20)) +  6h),  -90.0);
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/20)) + 12h),    0.0);
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/20)) + 18h),   90.0);
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/21)) +  0h -1s), 179.995833);
    TESTDOUBLE(solar_hour_angle(0, scl_timepoint(sys_days(year(2019)/3/21)) +  0h +1s), -179.995833);
    test_epsilon = math_epsilon;


    exit(EXIT_SUCCESS);
}
